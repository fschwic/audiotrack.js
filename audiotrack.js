/*
    audiotrack.js, v0.3
    by Frank Schwichtenberg
    with help from jonhall.info, paulbakaus.com, w3schools.com and others
    depends on jQuery and Bootstrap icons

    For playing audio tracks inside a webpage put a-tags with reference to a mp3 file
    and add CSS class 'audiotrack'. E.g.
    <a class="audio-track" href="music/my.mp3">My Song</a>
    Add this script at the end of the webpage. It will enhance those links (if HTML5 audio support is available). So, a 
    play icon is shown behind the link text and if you click it the music is played 
    "inside the page" instead replacing the page or downloding the music file.

    If there is one such link which has additionally the CSS class autoplay that audio
    track is started by the script. E.g.
    <a class="audio-track autoplay" href="music/my.mp3">My Song</a>

    Scince this is the first version, I just had a look on the functionality in
    most common browsers and only with MP3 audio files.
*/
(function(document) {
    console.log("audiotrack.js: Enhancing audio-track links!");

    var player; // html5 audio
    var context; // webAudio
    var src; // webAudio
    var supportsAudio = false;
    var webAudio = false; // set to true to force webAudio
    var iOSaudioOnlocked = false;

    function stopPlaying(){
        if(webAudio){
            src.stop(0);
        }
        else{
            player.pause();
        }
    }

    function startPlaying(url){
        if(webAudio){
            if(src){
                src.onended = function(e){
                    console.log("empty onended");
                    // ausgetrickst!
                }
                try{
                    src.stop(0);
                }
                catch(err){
                    console.log("Invalid state to call stop");
                }
            }
            src = context.createBufferSource();
            src.onended = function(e){
                console.log("on ended " + src.buffer);
                pauseAll();
            }

            url = "webaudionocache/" + url;
            console.log("Loading " + url);
            var req = new XMLHttpRequest(); 
            req.open("GET", url, true); 
            req.responseType = "arraybuffer"; 
            req.onload = function() { 
                //decode the loaded data 
                context.decodeAudioData(req.response, function(buffer) { 
                    src.buffer = buffer;
                    src.connect(context.destination);
                    src.start(0);
                }); 
            };
            req.send();
        }
        else{
            player.src = url;
            player.play();
        }
    }

    // create the player
    if ( webAudio || ((navigator.appVersion).match('OS 8_') && window.navigator.standalone) ){
        // don't use audio on iOS 8 in standalone mode
        //return false;
        console.log("Web Audio");
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        context = new AudioContext();

        supportsAudio = true;
        webAudio = true;
    }
    else{
        console.log("HTML5 Audio");
        player = document.createElement('audio');
        $('body').append('<audio id="the-player"></audio>');
        player = document.getElementById("the-player");

        supportsAudio = true;//!!player.canPlayType;
    }


    if (supportsAudio) {

        // set function to be executed if playing ends
        if(webAudio){
            // callback set before start playing
        }
        else{
            $(player).on("ended", function(e){
                pauseAll();
            });
        }

        // set all audio-tracks to paused state
        function pauseAll(){
            $('.audio-track').each(function(i, e){
                $(e).data("state", "paused");
                $(e).find('span').removeClass("glyphicon-pause").addClass("glyphicon-play");
            });
        }

        // enhance a tags which have class "audio-track"
        $('.audio-track').each(function(i, e){
            var text = $(e).data("text");
            if(text){
                //console.log("text:" + text);
                $(e).prepend("<strong>" + text + "</strong>");
            }
            $(e).append(' <span class="glyphicon glyphicon-play"></span>');
            $(e).on('click', function(e){
                var state = $(this).data("state");
                if(state == "playing"){
                    stopPlaying();
                    $(this).data("state", "paused");
                    $(this).find('span').removeClass("glyphicon-pause").addClass("glyphicon-play");
                }
                else{
                    startPlaying(this.getAttribute('href'));
                    // Set state for this to playing and for all others to paused.
                    pauseAll();
                    $(this).data("state", "playing");
                    $(this).find('span').removeClass("glyphicon-play").addClass("glyphicon-pause");
                }
                e.preventDefault();
            });
        });

        // autoplay only for html5 audio, currently
        if(!webAudio){
        // start playing if class "autoplay"
        if($('.audio-track.autoplay').length){
            console.log("audiotrack.js: Autoplay found!");
            //var text = $('.audio-track.autoplay').data("text");
            //$('.audio-track.autoplay').prepend("<strong>" + text + "</strong>");
            $(player).on("play", function(e){
                $('.audio-track.autoplay').data("state", "playing");
                $('.audio-track.autoplay').find('span').removeClass("glyphicon-play").addClass("glyphicon-pause");
                $(player).off("play");
            });
            player.autoplay=true;
            player.src = $('.audio-track.autoplay').attr('href');
        }
        }

    }
    else {
        console.log("No audio support detected. No audio-track link enhancement.");
    }

})(document);
